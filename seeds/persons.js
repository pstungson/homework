
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').del()
    .then(function () {
      // Inserts seed entries
      return knex('persons').insert([
        {name: "james", age: "18", gender: "male", created_at: knex.fn.now(), updated_at: knex.fn.now()},
        {name: "john", age: "20", gender: "male", created_at: knex.fn.now(), updated_at: knex.fn.now()},
      ]);
    });
};
