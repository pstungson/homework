import { PersonModel } from '../../models/person';

export const resolver = {
    Query: {
        getAllPersons: () => PersonModel.getAllPersons(),
        findPerson: (_, { id }) => PersonModel.findPerson(id),
    },
    Mutation: {
        createPerson(root, { input }) {
            return PersonModel.createPerson(input);
        },

        updatePerson(root, { id, input }) {
            return PersonModel.updatePerson(id, input);
        }
    }
}
